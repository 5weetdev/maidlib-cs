# MaidLib (C#)

A tool for game development without an editor ♥

## About the tool
**MaidLib** is a tool for creating your own games and applications. It should provide simple, convenient, and minimalistic development tools.

**MaidLib (C#)** is a tool for developing games in the **C# programming language** and using the **.NET Framework**.


### What does MaidLib-CS use?
First of all, we use [SDL2](https://www.libsdl.org/) to work with multimedia. To connect SDL2 to C#, we use the [SDL2-CS](https://github.com/flibitijibibo/SDL2-CS?tab=readme-ov-file) wapper.

**Here is a complete list of what we use:**

- [SDL2](https://www.libsdl.org/)

- [SDL2_image](https://github.com/libsdl-org/SDL_image)

- [SDL2_mixer](https://github.com/libsdl-org/SDL_mixer)

- [SDL2_ttf](https://github.com/libsdl-org/SDL_ttf)

### Why MaidLib?
MaidLib is a simple, intuitive, and functional tool for game development. It simplifies a large number of tasks, but remains flexible and does not force developers to follow specific rules. Your limit, your imagination

### Example C# Code
```cs
using maidLib; // Import MaidLib
using Tools; // Import addiadditional Tools like Vector2 or Size

namespace Basic;

class Program
{
    public static void Main()
    {
        int[] windowSize = {640, 480};
        Engine engine = new Engine(windowSize, iconPath: "./assets/ModernMaidLibIcon.png"); // Create Engine
        Scene1 scene = engine.AddScene<Scene1>(new Scene1()); // Add Scene1 to scene List
        engine.Run(); // Run program
    }
}

class Scene1 : Scene // Create scene
{
    public override void InitScene()
    {
        AddObjectToScene<Player>(new Player()); // Add GameObject(Player) to scene
    }
}

class Player : GameObject // Create GameObject(Player)
{
    private Transform _transform;
    private Sprite _sprite;

    public override void InitGameObject()
    {
        _transform = AddComponent<Transform>(new Transform(new Vector2(250, 120), new Size(80, 80))); // Add component "Transform"
        _sprite = AddComponent<Sprite>(new Sprite("./assets/MaidLibIcon.png")); // Add component "Sprite"
    }
}
```
This is a basic script that can be found at Template/Basic


## Requirements MaidLib-CS
- [.NET 6.0](https://dotnet.microsoft.com/en-us/download) or higher.

- (Optional) We recommend using Rider, VSCode, or Visual Studio to get the best experience with the tool

## Contributing
If you want to help the project, you can simply make **pull requests** to this project.

**Please do not make empty or bad commits. Really! Don't do that**

## About Developers
We are the MaidLib development team. Our goal is to support, develop, and popularize the MaidLib tool.

### Main Team:
TitleChanQWERTY (Bogdan Praporshikov)\
erthmaster

### Helpers
\_BrenD\_

*With love, MaidLib team*

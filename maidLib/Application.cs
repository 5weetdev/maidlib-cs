using Tools;

namespace maidLib;

public class Application
{
    public static bool IsRunning 
    {
        get;
        private set; 
    } = true;

    public static string WindowName
    {
        get;
        set;
    }
        
    public static Size WindowSize;

    public static string EngineVersion => "0";
    public static string EngineName => "MaidLib";

    public static IntPtr WindowSDL;

    public static IntPtr RendererSDL;

    public static void Quit()
    {
        IsRunning  = false;
    }
}
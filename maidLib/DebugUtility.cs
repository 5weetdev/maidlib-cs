namespace maidLib;

public static class DebugUtility
{
    public static void Log(object? message)
    {
        Console.ResetColor();
        Console.WriteLine(message);
    }

    public static void LogWarning(object? message)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public static void LogError(object? message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}
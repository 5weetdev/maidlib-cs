﻿using System.Text.Json;
using YamlDotNet;

namespace maidLib;

public static class ProjectSettings
{
    public static string ProjectName = "MaidLib game";
    public static string Company = "MaidLib team";
    public static string Version = "1.0.0";
    public static string IconPath = "./assets/ModernMaidLibIcon.png";
    public static int[] WindowSize = new int [2]{1000, 600};

    public static void Init()
    {
        Input.InputActions.Add("Horizontal", new List<Input.InputEvent>()
        {
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.D, 1),
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.A, -1)
        });
        Input.InputActions.Add("Vertical", new List<Input.InputEvent>()
        {
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.W, -1),
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.S, 1)
        });
        
        var inputEvent = new Input.InputEvent(Input.ActionType.MOUSE, MouseButton.LEFT, 1);
        
        var options = new JsonSerializerOptions { WriteIndented = true };
        DebugUtility.Log(JsonSerializer.Serialize(Input.InputActions, options));
        DebugUtility.Log(" \n \n \n" + Input.ActionType.MOUSE);
    }
}
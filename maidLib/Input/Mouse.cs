﻿using SDL2;
using Tools;

namespace maidLib;

public static class Mouse
{
    public static byte[] State;
    public static bool isButtonDown = false;
    public static bool isButtonUp = false;
    
    public static Vector2 Position { get; private set; }

    internal static void UpdatePosition()
    {
        SDL.SDL_GetMouseState(out var mouseX, out var mouseY);
    }
    
    public static bool GetMouseDown(MouseButton mouseButton)
    {
        return Event.MouseButton == mouseButton && isButtonDown;
    }
    
    public static bool GetMouseUp(MouseButton mouseButton)
    {
        return Event.MouseButton == mouseButton && isButtonUp;
    }
}

public enum MouseButton : uint
{
    LEFT = SDL.SDL_BUTTON_LEFT,
    RIGHT = SDL.SDL_BUTTON_RIGHT,
    MIDDLE = SDL.SDL_BUTTON_MIDDLE
}
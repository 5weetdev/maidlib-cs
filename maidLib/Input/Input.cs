﻿using Tools;

namespace maidLib;

public static class Input
{
    public static readonly Dictionary<string, List<InputEvent>?> InputActions = new();

    public static dynamic? GetInput<T>(string inputName)
    {
        if (!InputActions.TryGetValue(inputName, out var inputEvents))
            return default;

        var matchingEvent = inputEvents?.FirstOrDefault(inputEvent => 
            (inputEvent.ActionType == ActionType.KEYBOARD && Keyboard.GetKey((KeyCode)inputEvent.Input)) ||
            (inputEvent.ActionType == ActionType.MOUSE && Mouse.GetMouseDown((MouseButton)inputEvent.Input))
        );
        
        Type? eventType = matchingEvent?.ReturnValue?.GetType();
        if (matchingEvent?.Func == null)
            return matchingEvent?.ReturnValue ?? default(T);
        return matchingEvent?.Func?.Invoke() ??  default(T);
    }

    public static void Init()
    {
        InputActions.Add("MoveHorizontal", new List<InputEvent>()
        {
            new InputEvent(ActionType.KEYBOARD, KeyCode.D, Vector2.Right),
            new InputEvent(ActionType.KEYBOARD, KeyCode.A, Vector2.Left),
        });
        InputActions.Add("MoveVertical", new List<InputEvent>()
        {
            new InputEvent(ActionType.KEYBOARD, KeyCode.W, Vector2.Up),
            new InputEvent(ActionType.KEYBOARD, KeyCode.S, Vector2.Down)
        });
    }
    
    [Serializable]
    public struct InputEvent
    {
        public ActionType ActionType { get; }
        public object Input{ get; }
        public object? ReturnValue{ get; }
        public Func<object>? Func{ get; }
        
        public InputEvent(ActionType actionType, object input, object returnValue)
        {
            ActionType = actionType;
            Input = input;
            ReturnValue = returnValue;
            Func = null;
        }
        public InputEvent(ActionType actionType, object input, Func<object> func)
        {
            ActionType = actionType;
            Input = input;
            Func = func;
            ReturnValue = null;
        }
    }
    
    public enum ActionType
    {
        KEYBOARD,
        MOUSE
    }
}


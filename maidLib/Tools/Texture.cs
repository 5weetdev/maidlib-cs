using maidLib;
using SDL2;

namespace Tools;

class Texture
{
    private static readonly Dictionary<string, IntPtr> _textureCollect = new();
    
    public static IntPtr CreateTextureFromSurface(IntPtr surface)
    {
        IntPtr texture = SDL.SDL_CreateTextureFromSurface(Application.RendererSDL, surface);
        SDL.SDL_FreeSurface(surface);
        return texture;
    }

    public static IntPtr ChangeTextureColor(IntPtr texture, Color _color, Transform _transform)
    {
        SDL.SDL_DestroyTexture(texture);
        IntPtr surface = SDL.SDL_CreateRGBSurface(0, (int)_transform.Size.Width, (int)_transform.Size.Width, 32, 0, 0, 0, 0);
        UInt32 color = Color.MapRGB(_color, surface);
        SDL.SDL_FillRect(surface, IntPtr.Zero, color);
        return CreateTextureFromSurface(surface);
    }

    public static void ChangeTextureAlpha(IntPtr texture, Alpha _alpha)
    {
        SDL.SDL_SetTextureAlphaMod(texture, _alpha.A);
    }

    public static void DestroyAllTextures()
    {
        foreach (var texture in _textureCollect)
        {
            SDL.SDL_DestroyTexture(texture.Value);
        }
        _textureCollect.Clear();
    }
    
    public static IntPtr CreateTextureImage(string ImagePath, Transform _transform, Color _color)
    {
        IntPtr texture;
        if (_textureCollect.TryGetValue(ImagePath, out var image))
            return image;
        if (File.Exists(ImagePath))
        {
            texture = SDL_image.IMG_LoadTexture(Application.RendererSDL, ImagePath);
            var sdlError = SDL.SDL_GetError();
            if (string.IsNullOrEmpty(sdlError) == false)
            {
                DebugUtility.LogError(sdlError);
            }
        }
        else
        {
            IntPtr surface = SDL.SDL_CreateRGBSurface(0, (int)_transform.Size.Width, (int)_transform.Size.Width, 32, 0, 0, 0, 0);
            UInt32 color = Color.MapRGB(_color, surface);
            SDL.SDL_FillRect(surface, IntPtr.Zero, color);
            DebugUtility.LogError($"File \"{ImagePath}\" not exist");
            texture = CreateTextureFromSurface(surface);
        }
        _textureCollect.Add(ImagePath, texture);
        return texture;
    }
}

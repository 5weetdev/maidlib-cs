﻿namespace Tools;
using System;

public struct Vector2
{
    public float X;
    public float Y;
    
    public static Vector2 Zero = new(0, 0);
    public static Vector2 One = new(1, 1);
    public static Vector2 Left = new(-1, 0);
    public static Vector2 Right = new(1, 0);
    public static Vector2 Up = new(0, -1);
    public static Vector2 Down = new(0, 1);
    public readonly Vector2 Normalized => this / MathF.Sqrt(X*X + Y*Y);
    public readonly float Magnitude => MathF.Sqrt(X*X + Y*Y);

    public Vector2(float x, float y)
    {
        X = x;
        Y = y;
    }

    public Vector2(Vector2 vector) 
    {
        X = vector.X;
        Y = vector.Y;
    }

    public string GetString()
    {
        return $"(X: {X}, Y: {Y})";
    }
    
    public static Vector2 operator +(Vector2 v1, Vector2 v2)
    {
        return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
    }

    public static Vector2 operator -(Vector2 v1, Vector2 v2)
    {
        return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
    }
    
    public static Vector2 operator *(Vector2 v1, float number)
    {
        return new Vector2(v1.X * number, v1.Y * number);
    }

    public static Vector2 operator /(Vector2 v1, float number)
    {
        if (number == 0) return new Vector2(0,0); 
        return new Vector2(v1.X / number, v1.Y / number);
    }

    public static float Dot(Vector2 v1, Vector2 v2) 
    {
        return MathF.Sqrt(v1.X * v1.X + v2.Y * v2.Y); 
    }

    /// <param name="len1">Magnitude of first vector</param>
    /// <param name="len2">Magnitude of second vector</param>
    /// <param name="angle">Angle between vectors</param>
    public static float Dot(float len1, float len2, float angle) 
    {
        return len1 * len2 * MathF.Cos(angle);
    }

}
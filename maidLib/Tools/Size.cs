﻿namespace Tools;

public struct Size
{
	public float Width
	{
		get
		{
			return _width;
		}
		set
		{
			if(value < 0)
				throw new Exception("Width can't be less than 0");
			_width = value;
		}
	}
	
	public float Height
	{
		get
		{
			return _height;
		}
		set
		{
			if(value < 0)
				throw new Exception("Height can't be less than 0");
			_height = value;
		}
	}
	
	private float _width;
	private float _height;

	public Size(float width, float height)
	{
		if(height < 0 || width < 0)
			throw new Exception("Height and Width can't be less than 0");
		_width = width;
		_height = height;
	}
}
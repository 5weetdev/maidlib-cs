using SDL2;

namespace Tools;

public static class Time
{
    public static uint FPS
    {
        get => _fps;
        set
        {
            _sdlDesiredDelta = 1000 / value;
            _fps = value;
        }
    }

    private static uint _fps = 120;
    private static uint _sdlDesiredDelta = 1000 / FPS;

    internal static void LimitFps(uint startLoopTime)
    {
        uint delta = SDL.SDL_GetTicks() - startLoopTime;
        if (delta < _sdlDesiredDelta)
        {
            SDL.SDL_Delay(_sdlDesiredDelta - delta);
        }
        SDL.SDL_Delay(_sdlDesiredDelta);
    }
}
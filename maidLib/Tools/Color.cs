﻿using System.Runtime.InteropServices;
using SDL2;

namespace Tools;

public struct Color
{
    public byte R;
    public byte G;
    public byte B;
    
    public Color(byte red, byte green, byte blue)
    {
        R = red;
        G = green;
        B = blue;
    }

    public static UInt32 MapRGB(Color color, IntPtr surface)
    {
        return SDL.SDL_MapRGB(Marshal.PtrToStructure<SDL.SDL_Surface>(surface).format, color.R, color.G, color.B);
    }
}

public struct Alpha
{
    public byte A;
    
    public Alpha(byte alpha)
    {
        A = alpha;
    }
}

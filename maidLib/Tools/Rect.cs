using SDL2;

namespace Tools;

public class Rect
{
    public SDL.SDL_FRect _SdlRect;
    
    public Rect(float x, float y, float width, float height)
    {
        _SdlRect = CreateRect(x, y, width, height);
    }
    
    private SDL.SDL_FRect CreateRect(float xPos, float yPos, float width, float height)
    {
        return new SDL.SDL_FRect() {x = xPos, y = yPos, w = width, h = height};
    }

    public void UpdateRect(Vector2 position, Size size)
    {
        _SdlRect.x = position.X;
        _SdlRect.y = position.Y;
        _SdlRect.w = size.Width;
        _SdlRect.h = size.Height;
    }
}
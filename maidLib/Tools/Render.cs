using maidLib;
using SDL2;

namespace Tools;

class Render
{
    public static void RenderCopy(IntPtr texture, Transform? transform)
    {
        if (transform == null)
            return;
        
        SDL.SDL_SetTextureBlendMode(texture, SDL.SDL_BlendMode.SDL_BLENDMODE_BLEND);
        SDL.SDL_RenderCopyExF(Application.RendererSDL, texture, IntPtr.Zero, ref transform.Rect._SdlRect, transform.Rotation, IntPtr.Zero, SDL.SDL_RendererFlip.SDL_FLIP_NONE);
    }
}

﻿using maidLib;
using SDL2;

namespace Tools;

public static class Event
{
    public static MouseButton MouseButton { get; private set; }

    internal static readonly bool[] KeysPressed = new bool[(int)SDL.SDL_Scancode.SDL_NUM_SCANCODES];
    internal static readonly bool[] PreviousKeysPressed = new bool[(int)SDL.SDL_Scancode.SDL_NUM_SCANCODES];

    internal static void Finish()
    {
        Array.Copy(KeysPressed, PreviousKeysPressed, KeysPressed.Length);
    }

    internal static void HandleEvents()
    {
        SDL.SDL_PumpEvents();
        while (SDL.SDL_PollEvent(out SDL.SDL_Event e) != 0)
        {
            switch (e.type)
            {
                case SDL.SDL_EventType.SDL_QUIT:
                    Application.Quit();
                    break;
                case SDL.SDL_EventType.SDL_KEYDOWN:
                    KeysPressed[(int)e.key.keysym.scancode] = true;
                    break;
                case SDL.SDL_EventType.SDL_KEYUP:
                    KeysPressed[(int)e.key.keysym.scancode] = false;
                    break;
                case SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN:
                    Poll(ref e);
                    Mouse.isButtonDown = true;
                    break;
                case SDL.SDL_EventType.SDL_MOUSEBUTTONUP:
                    Poll(ref e);
                    Mouse.isButtonUp = true;
                    break;
            }
        }

        static void Poll(ref SDL.SDL_Event e)
        {
            MouseButton = (MouseButton)e.button.button;
        }
    }
}
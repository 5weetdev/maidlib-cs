using Tools;
using SDL2;

namespace maidLib;

/// <summary>
/// Made with love 💗 by
/// TitleChanQWERTY✪ and erthmaster
/// </summary>
public class Engine
{
    private readonly int _defaultSceneIndex;
    private readonly string _iconPath;

    public Engine(int[] windowSize, string nameWindow = "MaidLib", int defaultSceneIndex = 0, string iconPath = "")
    {
        Application.WindowName = nameWindow;
        Application.WindowSize = new Size(windowSize[0], windowSize[1]);
        _defaultSceneIndex = defaultSceneIndex;
        _iconPath = iconPath;
        InitEngine();
    }

    private void InitEngine()
    {
        SDL.SDL_Init(SDL.SDL_INIT_VIDEO);
        SDL_image.IMG_Init(SDL_image.IMG_InitFlags.IMG_INIT_PNG | SDL_image.IMG_InitFlags.IMG_INIT_JPG);
        Application.WindowSDL = SDL.SDL_CreateWindow(Application.WindowName, SDL.SDL_WINDOWPOS_CENTERED, SDL.SDL_WINDOWPOS_CENTERED, (int)Application.WindowSize.Width, (int)Application.WindowSize.Height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
        Application.RendererSDL = SDL.SDL_CreateRenderer(Application.WindowSDL, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED | SDL.SDL_RendererFlags.SDL_RENDERER_TARGETTEXTURE);
        
        SDL.SDL_Init(SDL.SDL_INIT_AUDIO);
        SDL_mixer.Mix_Init(SDL_mixer.MIX_InitFlags.MIX_INIT_MP3);
        if (SDL_mixer.Mix_OpenAudio(44100,SDL_mixer.MIX_DEFAULT_FORMAT,2,2048) < 0) 
        {
            DebugUtility.LogError($"There have occured problems with SDL_Mixer. Err: {SDL_mixer.Mix_GetError()}");
        } 

        if (!File.Exists(_iconPath)) return;
        IntPtr icon = SDL_image.IMG_Load(_iconPath);
        SDL.SDL_SetWindowIcon(Application.WindowSDL, icon);
        SDL.SDL_FreeSurface(icon);
    }

    public void Run()
    {
#if DEBUG
        Console.WriteLine($"\n-> {Application.EngineName}");
        Console.WriteLine($"-> Engine Version: {Application.EngineVersion}");
        Console.WriteLine($"\n=> Properties:");
        Console.WriteLine($"--> Window Size: {Application.WindowSize.Width},{Application.WindowSize.Height}");
        Console.WriteLine($"--> Window Name: {Application.WindowName}\n");
#endif
        Scene.MakeActive(Scene.Get(_defaultSceneIndex));
        Update();
    }

    private void UpdateCurrentScene()
    {
        if (Scene.Active == null)
            return;

        Color backgroundColor = Scene.Active.ColorBackground;
        SDL.SDL_SetRenderDrawColor(Application.RendererSDL,
            backgroundColor.R,
            backgroundColor.G,
            backgroundColor.B,
            0x0F);
        SDL.SDL_RenderClear(Application.RendererSDL);
        Scene.Active.UpdateAll();
        SDL.SDL_RenderPresent(Application.RendererSDL);
    }

    private void Update()
    {
        while (Application.IsRunning)
        {
            var startLoop = SDL.SDL_GetTicks();
            
            Scene.ChangeActiveScene();
            
            var keyboardStatePtr = SDL.SDL_GetKeyboardState(out var numKeys);
            Keyboard.State = new byte[numKeys];
            Mouse.State = new byte[5];
            System.Runtime.InteropServices.Marshal.Copy(keyboardStatePtr, Keyboard.State, 0, numKeys);

            Event.HandleEvents();
            
            UpdateCurrentScene();
            
            Mouse.UpdatePosition();
            Mouse.isButtonUp = false;
            Mouse.isButtonDown = false;

            Time.LimitFps(startLoop);
            Event.Finish();
        }

        Texture.DestroyAllTextures();
        SDL.SDL_DestroyWindow(Application.WindowSDL);
        SDL_image.IMG_Quit();
        SDL.SDL_Quit();
    }
}
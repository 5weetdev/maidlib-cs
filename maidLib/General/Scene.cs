using Tools;

namespace maidLib;

public class Scene
{
    public static Scene? Active => _scenes.Count == 0 || ActiveIndex < 0 ? null : _scenes[ActiveIndex];
    public static int ActiveIndex { get; private set; } = -1;
    
    private static int? _newActiveSceneIndex;
    private static readonly List<Scene> _scenes = new();

    public IReadOnlyList<GameObject> GameObjects => _gameObjects;
    public Color ColorBackground = new Color(0, 0, 0);
    
    private readonly List<GameObject> _gameObjectsToDestroy = new();
    private readonly List<GameObject> _gameObjects = new();

    public static Scene Get(int index)
    {
        return _scenes[index];
    }

    public static void MakeActive(int index)
    {
        if (index < 0 || index >= _scenes.Count)
            throw new Exception("Can't find scene");

        _newActiveSceneIndex = index == ActiveIndex ? null : index;
    }

    public static void MakeActive(Scene scene)
    {
        var index = _scenes.IndexOf(scene);
        MakeActive(index);
    }

    internal static void ChangeActiveScene()
    {
        if (_newActiveSceneIndex == null || _scenes.Count == 0)
            return;

        if (_newActiveSceneIndex.Value < 0)
            _newActiveSceneIndex = 0;
        
        Active?.DestroyScene();

        ActiveIndex = _newActiveSceneIndex.Value;
        Active!.InitScene();
        Active.Start();
        _newActiveSceneIndex = null;
    }
    
    public static T AddScene<T>(T scene) where T : Scene
    {
        if (scene == null)
            throw new Exception("Scene can't be null");
        _scenes.Add(scene);
        return scene;
    }
    
    public GameObject AddGameObject(GameObject gameObject)
    {
        if (gameObject == null) 
            throw new Exception("gameObject can't be null");
        
        gameObject.Scene = this;
        gameObject.InitGameObject();
        gameObject.Start();
        _gameObjects.Add(gameObject);
        return gameObject;
    }

    private void DestroyScene()
    {
        foreach (var gameObject in _gameObjects)
        {
            gameObject.ClearComponentList();
        }
        _gameObjectsToDestroy.Clear();
        _gameObjects.Clear();
    }

    internal void DestroyGameObject(GameObject gameObject)
    {
        _gameObjectsToDestroy.Add(gameObject);
    }

    public virtual void InitScene()
    {
        
    }
    
    public virtual void Start()
    {
        
    }

    public virtual void Update()
    {
        
    }
    
    public void UpdateAll()
    {
        int gameObjectCount = GameObjects.Count;
        Update();
        
        for (int index = 0; index < gameObjectCount; index++)
        {
            GameObjects[index].UpdateAll();
        }
        
        DestroyGameObjects();
    }

    private void DestroyGameObjects()
    {
        foreach (var gameObject in _gameObjectsToDestroy)
        {
            _gameObjects.Remove(gameObject);
        }   
        
        _gameObjectsToDestroy.Clear();
    }
}
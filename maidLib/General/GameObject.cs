namespace maidLib;

/// <summary>
/// Object that contains components
/// </summary>
public class GameObject
{
    public Scene? Scene { get; internal set; }
    public string Tag = "Untag";
    
    /// <summary>
    /// A list of game object`s components
    /// </summary>
    private readonly List<Component> _components = new();
    private readonly List<Component> _componentsToRemove = new();
    
    /// <summary>
    /// Adds component to the game object and returns it from
    /// </summary>
    /// <param name="component">Adding component instance</param>
    /// <typeparam name="T">Component to add</typeparam>
    /// <returns>Component</returns>
    /// <exception cref="Exception">component != null</exception>
    public T AddComponent<T>(T component) where T : Component
    {
        if (component == null) 
            throw new Exception("component can't be null");
            
        component.GameObject = this;
        component.InitComponent();
        component.Start();
        _components.Add(component);
        return component;
    }

    /// <summary>
    /// Gets component from the game object
    /// </summary>
    /// <typeparam name="T">Component to get</typeparam>
    /// <returns>GameObject`s component</returns>
    /// <exception cref="Exception">Component in GameObject`s components list != null</exception>
    public T? GetComponent<T>() where T : Component
    {
        foreach (Component? currentComponent in _components)
        {
            if (currentComponent?.GetType() == typeof(T))
            {
                return (T)currentComponent;
            }
        }
        
        return default;
    }
    
    public bool HasComponent<T>() where T : Component
    {
        foreach (Component? currentComponent in _components)
        {
            if (currentComponent?.GetType() == typeof(T))
            {
                return true;
            }
        }
        
        return false;
    }

    public bool TryGetComponent<T>(out T component) where T : Component
    {
        foreach (Component? currentComponent in _components)
        {
            if (currentComponent?.GetType() == typeof(T))
            {
                component = (T)currentComponent;
                return true;
            }
        }

        component = default!;
        return false;
    }

    /// <summary>
    /// Initializes game object (Usually uses for adding components)
    /// </summary>
    
    public void ClearComponentList()
    {
        _components.Clear();
    }
    
    public void RemoveComponent<T>() where T : Component
    {
        foreach (Component? currentComponent in _components)
        {
            if (currentComponent?.GetType() == typeof(T))
            {
                _componentsToRemove.Add(currentComponent);
                return;
            }
        }
        DebugUtility.LogError("Component" + typeof(T) + " not found at: " + this);
    }

    public void Destroy()
    {
        Scene?.DestroyGameObject(this);
    }
    
    public virtual void InitGameObject()
    {
            
    }

    /// <summary>
    /// Starts game object
    /// </summary>
    public virtual void Start()
    {

    }

    /// <summary>
    /// Updates user defined events
    /// </summary>
    public virtual void Update()
    {

    }

    /// <summary>
    /// Updates GameObject and all of its components
    /// </summary>
    public void UpdateAll()
    {
        int componentCount = _components.Count;
        Update();
        for (int index = 0; index < componentCount; index++)
        {
            _components[index].Update();
        }
        
        RemoveComponents();
    }

    private void RemoveComponents()
    {
        foreach (Component? currentComponent in _componentsToRemove)
        {
            _components.Remove(currentComponent);
        }
        
        _componentsToRemove.Clear();
    }

    public virtual void OnTriggerStay(Trigger2D trigger2D)
    {
        
    }
}

using JetBrains.Annotations;
using Tools;

namespace maidLib;

public class Sprite : Component
{
    public Color Color
    {
        get => _color;
        set
        {
            ChangeColor(value);
            _color = value;
        }
    }

    public Alpha Alpha
    {
        get => _alpha;
        set => _alpha = value;
    }
    
    private Transform _transform;
    private string _imagePath;
    private Color _color = new(220, 0, 220);
    private Alpha _alpha = new(255);
    
    private IntPtr _sprite;

    public Sprite([PathReference] string imagePath = "")
    {
        _imagePath = imagePath;
    }

    public override void InitComponent()
    {
        _transform = GameObject.GetComponent<Transform>()!;
        SetSprite(_imagePath);
    }

    public void SetSprite(string imagePath = "")
    {
        _imagePath = imagePath;
        _sprite = Texture.CreateTextureImage(_imagePath, _transform, this._color);
    }

    private void ChangeColor(Color color)
    {
        _sprite = Texture.ChangeTextureColor(_sprite, color, _transform);
    }
    
    private void ChangeAlpha(Alpha alpha)
    {
        Texture.ChangeTextureAlpha(_sprite, alpha);
    }
    
    public override void Update()
    {
        ChangeAlpha(Alpha);
        Render.RenderCopy(_sprite, _transform);
    }

}

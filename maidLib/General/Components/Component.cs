namespace maidLib;

public class Component
{
    public GameObject GameObject { get; internal set; }

    public virtual void InitComponent()
    {
        
    }

    public virtual void Start()
    {
            
    }

    public virtual void Update()
    {

    }
}
using SDL2;
using Tools;
namespace maidLib;

public class AudioSource : Component
{   
    private const int BASIC_VOLUME_VALUE = 50;
    private const int SOUND_PLAYING_CHANNEL = 0;
    private const int MUSIC_PLAYING_CHANNEL = 1;

    private readonly Dictionary<string, IntPtr> _musicStorage = new();
    private readonly Dictionary<string, IntPtr> _soundsStorage = new();
    private int _volume;

    public AudioSource()
    {
        SetVolume(BASIC_VOLUME_VALUE);
    }
    
    public AudioSource(int startVolume)
    {
        SetVolume(startVolume);
    }
    
    ~AudioSource() 
    {
        foreach (var item in _musicStorage) 
        {
            SDL_mixer.Mix_FreeMusic(item.Value);
        }
        
        foreach (var item in _soundsStorage) 
        {
            SDL_mixer.Mix_FreeChunk(item.Value);
        }
    }

    public void SetVolume(int volume) 
    {
        _volume = SDL_mixer.MIX_MAX_VOLUME * volume / 100;
    }
    
    public void LoadMusic(string filePath) 
    {
        IntPtr newMusicFile = SDL_mixer.Mix_LoadMUS(filePath);

        if (newMusicFile == IntPtr.Zero) 
        {
            DebugUtility.LogError($"Problems with loading file have arisen. Error: {SDL_mixer.Mix_GetError()}");
            return;
        }
        _musicStorage.Add(filePath,newMusicFile);
    }
    
    public void LoadSound(string filePath) 
    {
        IntPtr newSoundFile = SDL_mixer.Mix_LoadWAV(filePath);

        if (newSoundFile == IntPtr.Zero) 
        {
            DebugUtility.LogError($"Problems with loading SFX have arisen. Error: {SDL_mixer.Mix_GetError()}");
            return;
        }
        _soundsStorage.Add(filePath,newSoundFile);
    }
    
    public void PlayMusic(string filePath, int loops, bool clearQueueIfPlaying = default) 
    {
        if (_musicStorage.TryGetValue(filePath, out IntPtr value) == false) 
        {
            LoadMusic(filePath);
            value = _musicStorage[filePath];
            
            if (value == IntPtr.Zero) 
            {
                DebugUtility.LogError($"Problems with loading music have arisen. Error: {SDL_mixer.Mix_GetError()}");
                return;
            }
        }
        
        if (clearQueueIfPlaying && SDL_mixer.Mix_PlayingMusic() == 1)
        {
            DebugUtility.LogError("Some certain music is playing right now. Can't skip.");
            return;
        }

        SDL_mixer.Mix_HaltChannel(MUSIC_PLAYING_CHANNEL);

        SDL_mixer.Mix_Volume(MUSIC_PLAYING_CHANNEL, _volume);
        SDL_mixer.Mix_PlayMusic(value,loops);
    }
    
    public void PlaySound(string filePath) 
    {
        if (_soundsStorage.TryGetValue(filePath, out IntPtr value) == false) 
        {
            LoadSound(filePath);
            value = _soundsStorage[filePath];
            
            if (value == IntPtr.Zero) 
            {
                DebugUtility.LogError($"Problems with loading SFX have arisen. Error: {SDL_mixer.Mix_GetError()}");
                return;
            }
        }

        SDL_mixer.Mix_Volume(SOUND_PLAYING_CHANNEL, _volume);
        SDL_mixer.Mix_PlayChannel(SOUND_PLAYING_CHANNEL,value,0);
    }
    public void StopMusic() 
    {
        SDL_mixer.Mix_HaltChannel(MUSIC_PLAYING_CHANNEL);
    }
}

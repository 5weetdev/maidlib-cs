using SDL2;
using Tools;

namespace maidLib;

public class BoxCollider : Component
{
    public Rect RectCollider { get; private set; }
    
    private Transform _transform;
    private Size _size;
    
    public BoxCollider(Transform transform) : this(transform, transform.Size)
    {
        
    }
    
    public BoxCollider(Transform transform, Size size)
    {
        _transform = transform;
        _size = size;
        RectCollider = new Rect(_transform.Position.X, _transform.Position.Y, _size.Width, _size.Height);
    }
    

    public override void Update()
    {
        RectCollider.UpdateRect(_transform.Position, _size);
        CheckCollide();
    }

    private void CheckCollide()
    {
        foreach (GameObject collideObject in GameObject.Scene.GameObjects)
        {
            if (collideObject != GameObject && collideObject.TryGetComponent(out BoxCollider boxCollider))
            {
                SDL.SDL_FRect selfRect = RectCollider._SdlRect;
                SDL.SDL_FRect otherRect = boxCollider.RectCollider._SdlRect;
                if (selfRect.x + selfRect.w >= otherRect.x &&
                    otherRect.x + otherRect.w >= selfRect.x &&
                    selfRect.y + selfRect.h >= otherRect.y &&
                    otherRect.y + otherRect.h >= selfRect.y)
                {
                    GameObject.OnTriggerStay(new Trigger2D(collideObject));
                }
            }
        }
    }
}
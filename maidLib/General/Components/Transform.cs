using Tools;

namespace maidLib;

public class Transform : Component
{
    public Vector2 Position { get; set; }
    public Size Size { get; set; }
    public Vector2 Scale { get; set; }
    public double Rotation { get; set; }
    public Rect Rect { get; set; }

    public Transform(float x, float y, float width, float height)
    {
        Position = new Vector2(x, y);
        Scale = new Vector2(1, 1);
        Size = new Size(width, height);
        Rotation = 0;

        Rect = new Rect(x, y, width, height);

    }
    public Transform(Vector2 position, Size size)
    {
        Position = position;
        Scale = new Vector2(1, 1);
        Size = size;
        Rotation = 0;
        
        Rect = new Rect(Position.X, Position.Y, Size.Width, Size.Height);
    }

    public void Move(Vector2 direction)
    {
        Position += direction;
    }
    
    public override void Update()
    {
        Rect.UpdateRect(Position, Size);
    }
}

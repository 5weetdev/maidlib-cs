﻿using maidLib;
using Tools;

namespace Basic;

class Program
{
    public static void Main()
    {
        Input.InputActions.Add("Horizontal", new List<Input.InputEvent>()
        {
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.D, 1),
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.A, -1)
        });
        Input.InputActions.Add("Vertical", new List<Input.InputEvent>()
        {
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.W, -1),
            new Input.InputEvent(Input.ActionType.KEYBOARD, KeyCode.S, 1)
        });
        
        Engine engine = new Engine(ProjectSettings.WindowSize, iconPath: ProjectSettings.IconPath, nameWindow: ProjectSettings.ProjectName);
        Scene1 scene = Scene.AddScene(new Scene1());
        
        engine.Run();
    }
}

class Scene1 : Scene
{
    public override void InitScene()
    {
        ColorBackground = new Color(255, 255, 255);
        AddGameObject(new Player(120, 150));
    }
}

class Player : GameObject
{
    private Transform _transform;
    private Sprite _sprite;
    private BoxCollider _collider;
    private AudioSource _audioSource;

    private float Speed = 4.5f;
    private int _x;
    private int _y;

    private Random _random = new();

    public Player(int x, int y)
    {
        _x = x;
        _y = y;
    }

    public override void InitGameObject()
    {
        _transform = AddComponent(new Transform(new Vector2(_x, _y), new Size(80, 80)));
        _sprite = AddComponent(new Sprite("./assets/RobloxManFaceMaidLib.png"));
        _collider = AddComponent(new BoxCollider(_transform));
        _sprite.Alpha = new Alpha(100);

        _audioSource = AddComponent(new AudioSource());
    }

    public override void Update()
    {
        Vector2 moveDir = new Vector2(Vector2.Right * Input.GetInput<float>("Horizontal") + Vector2.Down * Input.GetInput<float>("Vertical")).Normalized;
        _transform.Move(new Vector2(moveDir * Speed));

        Input.GetInput<string>("Spawn");
        
        Speed = Keyboard.GetKey(KeyCode.LSHIFT) ? 40 : 5;
        
        if(Mouse.GetMouseDown(MouseButton.LEFT))
        { 
            Scene.AddGameObject(new Player2(_random.Next(0, 1000), _random.Next(0, 550)));
        }
        if (Keyboard.GetKey(KeyCode.R))
        {
            _transform.Rotation -= 1;
        }
        if (Keyboard.GetKey(KeyCode.T))
        {
            Alpha spriteAlpha = _sprite.Alpha;
            spriteAlpha.A += 1;
            _sprite.Alpha = spriteAlpha;
        }
        if (Keyboard.GetKeyDown(KeyCode.I)) 
        {
            _audioSource.PlayMusic("./assets/song.mp3",1,false);
        }
        if (Keyboard.GetKeyDown(KeyCode.O)) 
        {
            _audioSource.PlayMusic("./assets/song2.mp3",1,false);
        }
        if (Keyboard.GetKeyDown(KeyCode.P)) 
        {
            _audioSource.PlaySound("./assets/sound.wav");
        }

        Vector2 position = _transform.Position;
        position.X = Tools.Math.Clamp(position.X, 0, Application.WindowSize.Width-_transform.Size.Width);
        position.Y = Tools.Math.Clamp(position.Y, 0, Application.WindowSize.Height-_transform.Size.Height);
        _transform.Position = position;
    }
}

class Player2 : GameObject
{
    private Transform _transform;
    private Sprite _sprite;
    
    private int _speed = 2;

    private int _x;
    private int _y;

    public Player2(int x, int y)
    {
        _x = x;
        _y = y;
    }

    public override void InitGameObject()
    {
        _transform = AddComponent(new Transform(new Vector2(_x, _y), new Size(80, 80)));
        _sprite = AddComponent(new Sprite("./assets/RobloxManFaceMaidLib.png"));
    }
}
